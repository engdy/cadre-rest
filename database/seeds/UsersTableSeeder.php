<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = \Faker\Factory::create();
        $password = Hash::make('pastorius');
        User::create([
            'name' => 'Andy Foulke',
            'email' => 'engdy@engdy.net',
            'nick' => 'Engdy',
            'admin' => true,
            'password' => $password,
        ]);
        User::create([
            'name' => 'Andrew Foulke',
            'email' => 'AndyFoulke@gmail.com',
            'nick' => 'andy',
            'admin' => false,
            'password' => $password,
        ]);
        for ($i = 0; $i < 10; ++$i) {
            User::create([
                'name' => $faker->name,
                'nick' => $faker->firstName,
                'email' => $faker->email,
                'password' => $password,
                'admin' => false,
            ]);
        }
    }
}
